import frida
import sys
import time
import io
#代码可以直接写入下面，但是可读性不好，另外要增加setImmediate
jscode = """

"""
def printMessage(message,data):
    if message['type'] == 'send':
        print('[*] {0}'.format(message['payload']))
        # file_object = open("e:\\log.txt", 'ab+')
        # file_object.write(message['payload'].encode())
        # file_object.write('\n'.encode())
        # file_object.close()
    else:
        print(message)




# device = frida.get_usb_device()
# pid = device.spawn(["com.rom.testso"])
# device.resume(pid)
# time.sleep(8)
# process = device.attach(pid)

# device = frida.get_usb_device()
# front_app = device.get_frontmost_application()
# print (front_app)
# print (front_app.name)
# process = device.attach(front_app.name)
# ##process = frida.get_usb_device().attach('cpptest')
# ##直接读入sotest.js，在pycharm中代码有颜色感知，易于编辑
# with open('./sotestcpp.js',encoding='utf-8') as f:
#     jscode = f.read()
# script = process.create_script(jscode)
# script.on('message',printMessage)
# script.load()
# sys.stdin.read()


#当启动应用时进行Hook时，不要拦截别的函数了
#因为frida出名了，可能直接用默认端口有些app会识别到
device = frida.get_usb_device()
pid = device.spawn(['com.rom.cpptest'])  # app包名
process = device.attach(pid)  # 加载进程号
with open('./sotestcpp.js',encoding='utf-8') as f:
    jscode = f.read()
script = process.create_script(jscode)  # 创建js脚本
script.on('message', printMessage)  # 加载回调函数，也就是js中执行send函数规定要执行的python函数
script.load()  # 加载脚本
device.resume(pid)  # 重启app
sys.stdin.read()

#使用远程调用
# str_host = '127.0.0.1:8899'
# manager = frida.get_device_manager()
# device = manager.add_remote_device(str_host)
# pid = device.spawn(['com.rom.cpptest'])
# process = device.attach(pid)
# script = process.create_script(jscode)  # 创建js脚本
# script.on('message', printMessage)  # 加载回调函数，也就是js中执行send函数规定要执行的python函数
# script.load()  # 加载脚本
# device.resume(pid)
# sys.stdin.read()