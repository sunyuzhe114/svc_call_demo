#只针对armv7进行
# coding=utf-8
# coding=utf-8
import os
import re

import shutil
import binascii
import struct
import pathlib
from elftools.elf.elffile import  ELFFile
# hex_str=str_to_hexStr(r"com.termux/")
#root_path =  r'D:\working\AndroidLinux\ori_rom\extract\final'
#root_path = r'D:\working\android_linux\sermux\rom\from_rk3399\usr'
#root_path =r'D:\working\androidlinux\sermux\serumx-app\app\src\main\cpp\bootstrap-aarch64'
#            D:/working/AndroidLinux/sermux/rom/data/data/com.sermux/files/usr/etc/alternatives/vi|./bin/vi
#这个目录是从termux做好tar.gz文件解压缩后的romm目录

root_path =r'E:\frida\AntiFrida-master\app\build\outputs\apk\debug\app-debug\lib\armeabi-v7a'
root_path =r'D:\working\android\crack_huifeng\src\main\lib\armeabi-v7a'
root_path =r'E:\weixin8015_32\汇丰银行\lib\armeabi-v7a'
root_path =r'E:\weixin8015_32\lib\armeabi-v7a'
sysCallTab = {}
sysCallTab[0] ="__NR_restart_syscall";
sysCallTab[1] ="__NR_exit";
sysCallTab[2] ="__NR_fork";
sysCallTab[3] ="__NR_read";
sysCallTab[4] ="__NR_write";
sysCallTab[5] ="__NR_open";
sysCallTab[6] ="__NR_close";
sysCallTab[7] ="__NR_waitpid";
sysCallTab[8] ="__NR_creat";
sysCallTab[9] ="__NR_link";
sysCallTab[10] ="__NR_unlink";
sysCallTab[11] ="__NR_execve";
sysCallTab[12] ="__NR_chdir";
sysCallTab[13] ="__NR_time";
sysCallTab[14] ="__NR_mknod";
sysCallTab[15] ="__NR_chmod";
sysCallTab[16] ="__NR_lchown";
sysCallTab[17] ="__NR_break";
sysCallTab[18] ="__NR_oldstat";
sysCallTab[19] ="__NR_lseek";
sysCallTab[20] ="__NR_getpid";
sysCallTab[21] ="__NR_mount";
sysCallTab[22] ="__NR_umount";
sysCallTab[23] ="__NR_setuid";
sysCallTab[24] ="__NR_getuid";
sysCallTab[25] ="__NR_stime";
sysCallTab[26] ="__NR_ptrace";
sysCallTab[27] ="__NR_alarm";
sysCallTab[28] ="__NR_oldfstat";
sysCallTab[29] ="__NR_pause";
sysCallTab[30] ="__NR_utime";
sysCallTab[31] ="__NR_stty";
sysCallTab[32] ="__NR_gtty";
sysCallTab[33] ="__NR_access";
sysCallTab[34] ="__NR_nice";
sysCallTab[35] ="__NR_ftime";
sysCallTab[36] ="__NR_sync";
sysCallTab[37] ="__NR_kill";
sysCallTab[38] ="__NR_rename";
sysCallTab[39] ="__NR_mkdir";
sysCallTab[40] ="__NR_rmdir";
sysCallTab[41] ="__NR_dup";
sysCallTab[42] ="__NR_pipe";
sysCallTab[43] ="__NR_times";
sysCallTab[44] ="__NR_prof";
sysCallTab[45] ="__NR_brk";
sysCallTab[46] ="__NR_setgid";
sysCallTab[47] ="__NR_getgid";
sysCallTab[48] ="__NR_signal";
sysCallTab[49] ="__NR_geteuid";
sysCallTab[50] ="__NR_getegid";
sysCallTab[51] ="__NR_acct";
sysCallTab[52] ="__NR_umount2";
sysCallTab[53] ="__NR_lock";
sysCallTab[54] ="__NR_ioctl";
sysCallTab[55] ="__NR_fcntl";
sysCallTab[56] ="__NR_mpx";
sysCallTab[57] ="__NR_setpgid";
sysCallTab[58] ="__NR_ulimit";
sysCallTab[59] ="__NR_oldolduname";
sysCallTab[60] ="__NR_umask";
sysCallTab[61] ="__NR_chroot";
sysCallTab[62] ="__NR_ustat";
sysCallTab[63] ="__NR_dup2";
sysCallTab[64] ="__NR_getppid";
sysCallTab[65] ="__NR_getpgrp";
sysCallTab[66] ="__NR_setsid";
sysCallTab[67] ="__NR_sigaction";
sysCallTab[68] ="__NR_sgetmask";
sysCallTab[69] ="__NR_ssetmask";
sysCallTab[70] ="__NR_setreuid";
sysCallTab[71] ="__NR_setregid";
sysCallTab[72] ="__NR_sigsuspend";
sysCallTab[73] ="__NR_sigpending";
sysCallTab[74] ="__NR_sethostname";
sysCallTab[75] ="__NR_setrlimit";
sysCallTab[76] ="__NR_getrlimit";
sysCallTab[77] ="__NR_getrusage";
sysCallTab[78] ="__NR_gettimeofday";
sysCallTab[79] ="__NR_settimeofday";
sysCallTab[80] ="__NR_getgroups";
sysCallTab[81] ="__NR_setgroups";
sysCallTab[82] ="__NR_select";
sysCallTab[83] ="__NR_symlink";
sysCallTab[84] ="__NR_oldlstat";
sysCallTab[85] ="__NR_readlink";
sysCallTab[86] ="__NR_uselib";
sysCallTab[87] ="__NR_swapon";
sysCallTab[88] ="__NR_reboot";
sysCallTab[89] ="__NR_readdir";
sysCallTab[90] ="__NR_mmap";
sysCallTab[91] ="__NR_munmap";
sysCallTab[92] ="__NR_truncate";
sysCallTab[93] ="__NR_ftruncate";
sysCallTab[94] ="__NR_fchmod";
sysCallTab[95] ="__NR_fchown";
sysCallTab[96] ="__NR_getpriority";
sysCallTab[97] ="__NR_setpriority";
sysCallTab[98] ="__NR_profil";
sysCallTab[99] ="__NR_statfs";
sysCallTab[100] ="__NR_fstatfs";
sysCallTab[101] ="__NR_ioperm";
sysCallTab[102] ="__NR_socketcall";
sysCallTab[103] ="__NR_syslog";
sysCallTab[104] ="__NR_setitimer";
sysCallTab[105] ="__NR_getitimer";
sysCallTab[106] ="__NR_stat";
sysCallTab[107] ="__NR_lstat";
sysCallTab[108] ="__NR_fstat";
sysCallTab[109] ="__NR_olduname";
sysCallTab[110] ="__NR_iopl";
sysCallTab[111] ="__NR_vhangup";
sysCallTab[112] ="__NR_idle";
sysCallTab[113] ="__NR_vm86old";
sysCallTab[114] ="__NR_wait4";
sysCallTab[115] ="__NR_swapoff";
sysCallTab[116] ="__NR_sysinfo";
sysCallTab[117] ="__NR_ipc";
sysCallTab[118] ="__NR_fsync";
sysCallTab[119] ="__NR_sigreturn";
sysCallTab[120] ="__NR_clone";
sysCallTab[121] ="__NR_setdomainname";
sysCallTab[122] ="__NR_uname";
sysCallTab[123] ="__NR_modify_ldt";
sysCallTab[124] ="__NR_adjtimex";
sysCallTab[125] ="__NR_mprotect";
sysCallTab[126] ="__NR_sigprocmask";
sysCallTab[127] ="__NR_create_module";
sysCallTab[128] ="__NR_init_module";
sysCallTab[129] ="__NR_delete_module";
sysCallTab[130] ="__NR_get_kernel_syms";
sysCallTab[131] ="__NR_quotactl";
sysCallTab[132] ="__NR_getpgid";
sysCallTab[133] ="__NR_fchdir";
sysCallTab[134] ="__NR_bdflush";
sysCallTab[135] ="__NR_sysfs";
sysCallTab[136] ="__NR_personality";
sysCallTab[137] ="__NR_afs_syscall";
sysCallTab[138] ="__NR_setfsuid";
sysCallTab[139] ="__NR_setfsgid";
sysCallTab[140] ="__NR__llseek";
sysCallTab[141] ="__NR_getdents";
sysCallTab[142] ="__NR__newselect";
sysCallTab[143] ="__NR_flock";
sysCallTab[144] ="__NR_msync";
sysCallTab[145] ="__NR_readv";
sysCallTab[146] ="__NR_writev";
sysCallTab[147] ="__NR_getsid";
sysCallTab[148] ="__NR_fdatasync";
sysCallTab[149] ="__NR__sysctl";
sysCallTab[150] ="__NR_mlock";
sysCallTab[151] ="__NR_munlock";
sysCallTab[152] ="__NR_mlockall";
sysCallTab[153] ="__NR_munlockall";
sysCallTab[154] ="__NR_sched_setparam";
sysCallTab[155] ="__NR_sched_getparam";
sysCallTab[156] ="__NR_sched_setscheduler";
sysCallTab[157] ="__NR_sched_getscheduler";
sysCallTab[158] ="__NR_sched_yield";
sysCallTab[159] ="__NR_sched_get_priority_max";
sysCallTab[160] ="__NR_sched_get_priority_min";
sysCallTab[161] ="__NR_sched_rr_get_interval";
sysCallTab[162] ="__NR_nanosleep";
sysCallTab[163] ="__NR_mremap";
sysCallTab[164] ="__NR_setresuid";
sysCallTab[165] ="__NR_getresuid";
sysCallTab[166] ="__NR_vm86";
sysCallTab[167] ="__NR_query_module";
sysCallTab[168] ="__NR_poll";
sysCallTab[169] ="__NR_nfsservctl";
sysCallTab[170] ="__NR_setresgid";
sysCallTab[171] ="__NR_getresgid";
sysCallTab[172] ="__NR_prctl";
sysCallTab[173] ="__NR_rt_sigreturn";
sysCallTab[174] ="__NR_rt_sigaction";
sysCallTab[175] ="__NR_rt_sigprocmask";
sysCallTab[176] ="__NR_rt_sigpending";
sysCallTab[177] ="__NR_rt_sigtimedwait";
sysCallTab[178] ="__NR_rt_sigqueueinfo";
sysCallTab[179] ="__NR_rt_sigsuspend";
sysCallTab[180] ="__NR_pread64";
sysCallTab[181] ="__NR_pwrite64";
sysCallTab[182] ="__NR_chown";
sysCallTab[183] ="__NR_getcwd";
sysCallTab[184] ="__NR_capget";
sysCallTab[185] ="__NR_capset";
sysCallTab[186] ="__NR_sigaltstack";
sysCallTab[187] ="__NR_sendfile";
sysCallTab[188] ="__NR_getpmsg";
sysCallTab[189] ="__NR_putpmsg";
sysCallTab[190] ="__NR_vfork";
sysCallTab[191] ="__NR_ugetrlimit";
sysCallTab[192] ="__NR_mmap2";
sysCallTab[193] ="__NR_truncate64";
sysCallTab[194] ="__NR_ftruncate64";
sysCallTab[195] ="__NR_stat64";
sysCallTab[196] ="__NR_lstat64";
sysCallTab[197] ="__NR_fstat64";
sysCallTab[198] ="__NR_lchown32";
sysCallTab[199] ="__NR_getuid32";
sysCallTab[200] ="__NR_getgid32";
sysCallTab[201] ="__NR_geteuid32";
sysCallTab[202] ="__NR_getegid32";
sysCallTab[203] ="__NR_setreuid32";
sysCallTab[204] ="__NR_setregid32";
sysCallTab[205] ="__NR_getgroups32";
sysCallTab[206] ="__NR_setgroups32";
sysCallTab[207] ="__NR_fchown32";
sysCallTab[208] ="__NR_setresuid32";
sysCallTab[209] ="__NR_getresuid32";
sysCallTab[210] ="__NR_setresgid32";
sysCallTab[211] ="__NR_getresgid32";
sysCallTab[212] ="__NR_chown32";
sysCallTab[213] ="__NR_setuid32";
sysCallTab[214] ="__NR_setgid32";
sysCallTab[215] ="__NR_setfsuid32";
sysCallTab[216] ="__NR_setfsgid32";
sysCallTab[217] ="__NR_pivot_root";
sysCallTab[218] ="__NR_mincore";
sysCallTab[219] ="__NR_madvise";
sysCallTab[220] ="__NR_getdents64";
sysCallTab[221] ="__NR_fcntl64";
sysCallTab[224] ="__NR_gettid";
sysCallTab[225] ="__NR_readahead";
sysCallTab[226] ="__NR_setxattr";
sysCallTab[227] ="__NR_lsetxattr";
sysCallTab[228] ="__NR_fsetxattr";
sysCallTab[229] ="__NR_getxattr";
sysCallTab[230] ="__NR_lgetxattr";
sysCallTab[231] ="__NR_fgetxattr";
sysCallTab[232] ="__NR_listxattr";
sysCallTab[233] ="__NR_llistxattr";
sysCallTab[234] ="__NR_flistxattr";
sysCallTab[235] ="__NR_removexattr";
sysCallTab[236] ="__NR_lremovexattr";
sysCallTab[237] ="__NR_fremovexattr";
sysCallTab[238] ="__NR_tkill";
sysCallTab[239] ="__NR_sendfile64";
sysCallTab[240] ="__NR_futex";
sysCallTab[241] ="__NR_sched_setaffinity";
sysCallTab[242] ="__NR_sched_getaffinity";
sysCallTab[243] ="__NR_set_thread_area";
sysCallTab[244] ="__NR_get_thread_area";
sysCallTab[245] ="__NR_io_setup";
sysCallTab[246] ="__NR_io_destroy";
sysCallTab[247] ="__NR_io_getevents";
sysCallTab[248] ="__NR_io_submit";
sysCallTab[249] ="__NR_io_cancel";
sysCallTab[250] ="__NR_fadvise64";
sysCallTab[252] ="__NR_exit_group";
sysCallTab[253] ="__NR_lookup_dcookie";
sysCallTab[254] ="__NR_epoll_create";
sysCallTab[255] ="__NR_epoll_ctl";
sysCallTab[256] ="__NR_epoll_wait";
sysCallTab[257] ="__NR_remap_file_pages";
sysCallTab[258] ="__NR_set_tid_address";
sysCallTab[259] ="__NR_timer_create";
sysCallTab[260] ="__NR_timer_settime";
sysCallTab[261] ="__NR_timer_gettime";
sysCallTab[262] ="__NR_timer_getoverrun";
sysCallTab[263] ="__NR_timer_delete";
sysCallTab[264] ="__NR_clock_settime";
sysCallTab[265] ="__NR_clock_gettime";
sysCallTab[266] ="__NR_clock_getres";
sysCallTab[267] ="__NR_clock_nanosleep";
sysCallTab[268] ="__NR_statfs64";
sysCallTab[269] ="__NR_fstatfs64";
sysCallTab[270] ="__NR_tgkill";
sysCallTab[271] ="__NR_utimes";
sysCallTab[272] ="__NR_fadvise64_64";
sysCallTab[273] ="__NR_vserver";
sysCallTab[274] ="__NR_mbind";
sysCallTab[275] ="__NR_get_mempolicy";
sysCallTab[276] ="__NR_set_mempolicy";
sysCallTab[277] ="__NR_mq_open";
sysCallTab[278] ="__NR_mq_unlink";
sysCallTab[279] ="__NR_mq_timedsend";
sysCallTab[280] ="__NR_mq_timedreceive";
sysCallTab[281] ="__NR_mq_notify";
sysCallTab[282] ="__NR_mq_getsetattr";
sysCallTab[283] ="__NR_kexec_load";
sysCallTab[284] ="__NR_waitid";
sysCallTab[286] ="__NR_add_key";
sysCallTab[287] ="__NR_request_key";
sysCallTab[288] ="__NR_keyctl";
sysCallTab[289] ="__NR_ioprio_set";
sysCallTab[290] ="__NR_ioprio_get";
sysCallTab[291] ="__NR_inotify_init";
sysCallTab[292] ="__NR_inotify_add_watch";
sysCallTab[293] ="__NR_inotify_rm_watch";
sysCallTab[294] ="__NR_migrate_pages";
sysCallTab[295] ="__NR_openat";
sysCallTab[296] ="__NR_mkdirat";
sysCallTab[297] ="__NR_mknodat";
sysCallTab[298] ="__NR_fchownat";
sysCallTab[299] ="__NR_futimesat";
sysCallTab[300] ="__NR_fstatat64";
sysCallTab[301] ="__NR_unlinkat";
sysCallTab[302] ="__NR_renameat";
sysCallTab[303] ="__NR_linkat";
sysCallTab[304] ="__NR_symlinkat";
sysCallTab[305] ="__NR_readlinkat";
sysCallTab[306] ="__NR_fchmodat";
sysCallTab[307] ="__NR_faccessat";
sysCallTab[308] ="__NR_pselect6";
sysCallTab[309] ="__NR_ppoll";
sysCallTab[310] ="__NR_unshare";
sysCallTab[311] ="__NR_set_robust_list";
sysCallTab[312] ="__NR_get_robust_list";
sysCallTab[313] ="__NR_splice";
sysCallTab[314] ="__NR_sync_file_range";
sysCallTab[315] ="__NR_tee";
sysCallTab[316] ="__NR_vmsplice";
sysCallTab[317] ="__NR_move_pages";
sysCallTab[318] ="__NR_getcpu";
sysCallTab[319] ="__NR_epoll_pwait";
sysCallTab[320] ="__NR_utimensat";
sysCallTab[321] ="__NR_signalfd";
sysCallTab[322] ="__NR_timerfd_create";
sysCallTab[323] ="__NR_eventfd";
sysCallTab[324] ="__NR_fallocate";
sysCallTab[325] ="__NR_timerfd_settime";
sysCallTab[326] ="__NR_timerfd_gettime";
sysCallTab[327] ="__NR_signalfd4";
sysCallTab[328] ="__NR_eventfd2";
sysCallTab[329] ="__NR_epoll_create1";
sysCallTab[330] ="__NR_dup3";
sysCallTab[331] ="__NR_pipe2";
sysCallTab[332] ="__NR_inotify_init1";
sysCallTab[333] ="__NR_preadv";
sysCallTab[334] ="__NR_pwritev";
sysCallTab[335] ="__NR_rt_tgsigqueueinfo";
sysCallTab[336] ="__NR_perf_event_open";
sysCallTab[337] ="__NR_recvmmsg";
sysCallTab[338] ="__NR_fanotify_init";
sysCallTab[339] ="__NR_fanotify_mark";
sysCallTab[340] ="__NR_prlimit64";
sysCallTab[341] ="__NR_name_to_handle_at";
sysCallTab[342] ="__NR_open_by_handle_at";
sysCallTab[343] ="__NR_clock_adjtime";
sysCallTab[344] ="__NR_syncfs";
sysCallTab[345] ="__NR_sendmmsg";
sysCallTab[346] ="__NR_setns";
sysCallTab[347] ="__NR_process_vm_readv";
sysCallTab[348] ="__NR_process_vm_writev";
sysCallTab[349] ="__NR_kcmp";
sysCallTab[350] ="__NR_finit_module";
sysCallTab[351] ="__NR_sched_setattr";
sysCallTab[352] ="__NR_sched_getattr";
sysCallTab[353] ="__NR_renameat2";
sysCallTab[354] ="__NR_seccomp";
sysCallTab[355] ="__NR_getrandom";
sysCallTab[356] ="__NR_memfd_create";
sysCallTab[357] ="__NR_bpf";
sysCallTab[358] ="__NR_execveat";
sysCallTab[359] ="__NR_socket";
sysCallTab[360] ="__NR_socketpair";
sysCallTab[361] ="__NR_bind";
sysCallTab[362] ="__NR_connect";
sysCallTab[363] ="__NR_listen";
sysCallTab[364] ="__NR_accept4";
sysCallTab[365] ="__NR_getsockopt";
sysCallTab[366] ="__NR_setsockopt";
sysCallTab[367] ="__NR_getsockname";
sysCallTab[368] ="__NR_getpeername";
sysCallTab[369] ="__NR_sendto";
sysCallTab[370] ="__NR_sendmsg";
sysCallTab[371] ="__NR_recvfrom";
sysCallTab[372] ="__NR_recvmsg";
sysCallTab[373] ="__NR_shutdown";
sysCallTab[374] ="__NR_userfaultfd";
sysCallTab[375] ="__NR_membarrier";
sysCallTab[376] ="__NR_mlock2";


def get_file_path(root_path,file_list,dir_list):
    #获取该目录下所有的文件名称和目录名称
    dir_or_files = os.listdir(root_path)
    for dir_file in dir_or_files:
        #获取目录或者文件的路径
        dir_file_path = os.path.join(root_path,dir_file)
        #判断该路径为文件还是路径
        if os.path.isdir(dir_file_path):
            dir_list.append(dir_file_path)
            #递归获取所有文件和目录的路径
            get_file_path(dir_file_path,file_list,dir_list)
        else:
            file_list.append(dir_file_path)

def alter(file, old_str, new_str):
    """
    替换文件中的字符串
    :param file:文件名
    :param old_str:就字符串
    :param new_str:新字符串
    :return:
    """
    try:
        file_data = ""
        f = open(file, 'rb').read()
        file_data=f.decode()
        find_info=False
        if old_str in file_data:
            file_data = file_data.replace(old_str, new_str)
            find_info=True
        if find_info:
            with open(file, "w",newline="") as f:
                f.write(file_data)

            print( file, "ok")
            #shutil.copy(file ,r"D:\pro\termux")
        else:
            #print(file, "error")
            pass

    except Exception as e:
        #print(file,"error")
        pass


extfun = lambda x: x


def read_file_hex(file_path):
    file_object = open(file_path, 'rb')
    file_object.seek(0, 0)
    hex_str = ''
    byte = file_object.read()
    if byte:
        for b in byte:
            b=b.to_bytes(length=1, byteorder='big', signed=False)
            hex_str += ('%02X' % extfun(ord(b)))
    file_object.close()
    return hex_str


def wirte_to_file(hex, file_path):
    fout = open(file_path, 'wb')
    fileLength=(int)(len(hex) / 2)
    for i in range( fileLength):
        x = int(hex[2 * i:2 * (i + 1)], 16)
        fout.write(struct.pack('B', extfun(x)))
    fout.close()


def hex_replace(hex, find_str, replace_str):
    return hex.replace(find_str, replace_str)


# 获取目录下的文件
def file_name(file_dir):
    for root, dirs, files in os.walk(file_dir):
        return (files)

# 获取目录下的目录
def file_dir(file_dir):
    for root, dirs, files in os.walk(file_dir):
        return (dirs)

# 获取后缀名
def file_extension(file):
    return os.path.splitext(file)[1]

# 获取后缀名
def str_to_hexStr(string):
    str_bin = string.encode('utf-8')
    return binascii.hexlify(str_bin).decode('utf-8').upper()

import time

start = time.time()

src_path = root_path
dst_path = root_path

#用来存放所有的文件路径
file_list = []
#用来存放所有的目录路径
dir_list = []
get_file_path(root_path,file_list,dir_list)
totalchangedfiles=0
for idx, file_path in enumerate(file_list):
    #alter(file_path, b'termux/', b'sermux/')
    if file_extension(file_path) == ".so":
        try:
            if("libdetection_based_tracker.so" in file_path):
                findfile=1
            #将文件转为十六进制字串，便于检索
            file_str = read_file_hex(file_path)
            # 获取文件大小
            filesize= os.path.getsize(file_path)
            # 获取文件行数，每32个字为一行，打印
            #linesize=int(filesize/32)-1
            # for i in range(0,linesize):
            #     text_list = re.findall(".{2}", file_str[i*32+0:i*32+32])
            #     new_text = " ".join(text_list)
            #     print(hex(i*16).replace("0x","")+"h",new_text)
            #wirte_to_file(file_str,file_path+".so")


            textStart = 0
            textEnd = 0
            FindtextEnd =False
            #读取elf文件信息，计算代码的地址与终止段 ,取section.name=='.text'后，马上取下一个就是textEnd
            #
            with open(file_path,'rb') as f:
                e=ELFFile(f)
                for section in e.iter_sections():
                    #打印Elf各段地址
                    #print(section['sh_addr'],hex(section['sh_addr']),section.name)
                    if FindtextEnd is True:
                        textEnd = section['sh_addr']
                        break
                    if(section.name=='.text'):
                        textStart=section['sh_addr']
                        FindtextEnd=True
                    # if(section.name=='.ARM.exidx'):
                    #     if(textEnd ==0  or textEnd>section['sh_addr'] ):
                    #         textEnd=section['sh_addr']
                    # if(section.name=='.rodata'):
                    #     if(textEnd ==0  or textEnd>section['sh_addr'] ):
                    #         textEnd = section['sh_addr']


            # hex_txt_file = file_path + ".txt"
            # with open(hex_txt_file, "w") as file:  # 只需要将之前的”w"改为“a"即可，代表追加内容
            #     file.write(file_str )
            #     print("generate",hex_txt_file)

            # 微信应用是这个svc 0 =>00DF
            sub = "00DF"
            addr = [substr.start() for substr in re.finditer(sub, file_str)]
            total_svc=0
            for i in addr:
                if( i >= textStart and  i< textEnd):
                    #之前有问题是因为地址没对齐，比如 E0 0E F1 转成字串时 E00EF1

                    # 也包含了00EF,这就要用地址必须为偶数
                    # 4FF0140700DF
                    m=int(i/2)-6
                    if(i % 4==0):

                        fun_id  = int("0x"+file_str[i - 4:i - 2],16)
                        fun_id_check = int("0x" + file_str[i - 2:i ], 16)
                        if(fun_id_check ==0x7):
                            total_svc = total_svc + 1
                            if (fun_id in sysCallTab):
                                print(file_str[i - 8:i + 4], hex(fun_id),
                                      "addr : 0x%.8x       Func Name : %s     " % (m, sysCallTab[fun_id]))
                            else:
                                print(file_str[i - 8:i + 4], hex(fun_id),
                                      "addr : 0x%.8x       Func Name : need check again*********    " % (m))
            #print(os.path.basename(file_path), "totoal find svc call ", total_svc)
            if (total_svc > 0):
                print(os.path.basename(file_path), "elf infor ")
                print(os.path.basename(file_path), ".text start ", hex(textStart), ".text end ", hex(textEnd))
                print(os.path.basename(file_path), "find svc call ", total_svc)
                print("——————————————————————————————\n")
            #我们自己应用是这个svc 0 =>000000EF
            # sub="000000EF"
            # addr = [substr.start() for substr in re.finditer(sub, file_str)]
            # total_svc = 0
            #
            # for i in addr:
            #     if( i >= textStart and  i< textEnd):
            #         funid1 = int("0x"+file_str[i - 8:i - 6],16)
            #         funid2 = int("0x" + file_str[i - 6:i - 4], 16) -0x70
            #         fun_id=funid2*256+funid1
            #         m=int(i/2)-4
            #         if (i % 8 == 0):
            #             total_svc = total_svc + 1
            #             if fun_id>0:
            #                 #还有一种异常 B8 FF 00 00   00 FF 02 1F  是两个命令的头尾
            #                 if(  fun_id  in sysCallTab):
            #                     print(file_str[i-8:i+8],hex(fun_id),"  Func Name : %s     addr : 0x%x" % (sysCallTab[fun_id],m ))
            #                 else:
            #                     print(file_str[i - 8:i + 8], hex(fun_id),"  Func Name : need check     addr : 0x%x" % (  m))
            #
            #             else:
            #                 print(file_str[i-8:i+8],hex(fun_id),"  Func Name : needtocheck     addr : 0x%x" % (m ))

            # if( total_svc >0):
            #     print(os.path.basename(file_path), "elf infor ")
            #     print(os.path.basename(file_path), ".text start ", hex(textStart), ".text end ", hex(textEnd))
            #     print(os.path.basename(file_path), "find svc call ", total_svc)
            #     print("——————————————————————————————\n")


        except Exception as e:
            print(file_path, "--------------------------->error")
            pass


        totalchangedfiles = totalchangedfiles + 1

        finalpath = dst_path + file_path.replace(root_path, "")
        finalDir = os.path.dirname(finalpath)
        #finalDir = pathlib.Path(finalpath)
        if  os.path.exists(finalDir):
            pass
        else:
            os.makedirs(finalDir)
            pass
        #shutil.copy(file_path, finalpath)
            #print(file_path, "--------------------------->", finalpath)
            #   pass


print( "\n--------------------------->finished<--------------------------")
end = time.time()
running_time = end-start