import frida  # 导入frida模块
import sys  # 导入sys模块
#adb forward tcp:8899 tcp:8899
#/data/local/tmp/frida-server -l 0.0.0.0:8899
#frida-ps -H 127.0.0.1:8899
#frida -H 127.0.0.1:8899 cpptest -l sotestcpp.js
#objection  -N  -h 127.0.0.1 -p 8899 -g cpptest explore
jscode = """
function hook_OnCreate()
{
    Java.perform(function()
    {
        var MainActivity = Java.use("com.rom.cpptest.MainActivity"); // 类的加载路径

        MainActivity.onCreate.overload('android.os.Bundle').implementation = function(str){
            send("success1");
            this.onCreate(str);
            send("success2 "+str);
        };
    });
}
setImmediate(hook_OnCreate);
"""
def on_message(message, data):  # js中执行send函数后要回调的函数
    if message["type"] == "send":
        print("[*] {0}".format(message["payload"]))
    else:
        print(message)
#使用usb调用
# device = frida.get_usb_device()
# pid = device.spawn(['com.rom.cpptest'])  # app包名
# process = device.attach(pid)  # 加载进程号
# script = process.create_script(jscode)  # 创建js脚本
# script.on('message', on_message)  # 加载回调函数，也就是js中执行send函数规定要执行的python函数
# script.load()  # 加载脚本
# device.resume(pid)  # 重启app
# sys.stdin.read()

#使用远程调用
str_host = '127.0.0.1:8899'
manager = frida.get_device_manager()
device = manager.add_remote_device(str_host)
pid = device.spawn(['com.rom.cpptest'])
process = device.attach(pid)
script = process.create_script(jscode)  # 创建js脚本
script.on('message', on_message)  # 加载回调函数，也就是js中执行send函数规定要执行的python函数
script.load()  # 加载脚本
device.resume(pid)
sys.stdin.read()