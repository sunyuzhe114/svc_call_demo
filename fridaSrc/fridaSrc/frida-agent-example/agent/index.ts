import { log } from "./logger";
import { hookSyscall } from '../../frida-syscall-interceptor';
import { types } from "util";

/*
const header = Memory.alloc(16);
header
    .writeU32(0xdeadbeef).add(4)
    .writeU32(0xd00ff00d).add(4)
    .writeU64(uint64("0x1122334455667788"));
log(hexdump(header.readByteArray(16) as ArrayBuffer, { ansi: true }));

Process.getModuleByName("libSystem.B.dylib")
    .enumerateExports()
    .slice(0, 16)
    .forEach((exp, index) => {
        log(`export ${index}: ${exp.name}`);
    });

Interceptor.attach(Module.getExportByName(null, "open"), {
    onEnter(args) {
        const path = args[0].readUtf8String();
        log(`open() path="${path}"`);
    }
});
*/

// Somewhere in your code.

//*
let baseAddr = Module.findBaseAddress('libnative-lib.so')!;
let address = baseAddr.add('0x81F8');   //  00 00 00 EF  SVC  0  指令的地址
 
log(Process.arch);

hookSyscall(address, new NativeCallback(function (dirfd, pathname, mode, flags) {
    let path = pathname.readCString();

    log(`Called openat hook`);
    log('- R0: ' + dirfd);
    log('- R1: ' + path);
    log('- R2: ' + mode);
    log('- R3: ' + flags);

    return 0;
}, 'int', ['int', 'pointer', 'int', 'int']));
// */



// frida -U -l _agent.js com.fenfei.syscalldemo